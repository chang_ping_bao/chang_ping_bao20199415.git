#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void menu();
void help();
void error();
void grade_one();
void grade_two();
void grade_three();
void operation_one();
void operation_two();
void operation_three();
int main()
{
	int n;
	printf("----------口算生成器------------\n欢迎使用口算生成器:\n");
	printf("\n");
	help();
	while(1)
	{
		menu();
		scanf("%d",&n);
		switch(n)
		{
			case 1:
				grade_one();break;
			case 2:
				grade_two();break;
			case 3:
				grade_three();break;
			case 4:
				help();break;
		}
		printf("\n");
			if(n==5) break;
		if(n>5||n<1) error();
	}
	return 0;
}
void help()
{
	printf("帮助信息\n您需要输入命令代码来进行操作,且\n");
	printf("一年级题目为不超过十位数的加减法;\n二年级题目不超过百位的乘数法;\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
	printf("\n"); 
}
void menu()
{
	printf("操作表列:\n1)一年级   2)二年级   3)三年级\n4)帮助   5)退出程序\n请输入代号:"); 
}
void error()
{
	printf("请输入正确数值.");
	printf("\n");
	printf("\n");
}
void grade_one()
{
	printf("一年级题目如下:\n");
	operation_one(); 
}
void grade_two()
{
	printf("二年级题目如下:\n");
	operation_two();
}
void grade_three()
{
	printf("三年级题目如下:\n");
	operation_three(); 
}
void operation_one()
{
	int n,i;
	printf("请输入需要的题目:\n");
	char flag[2]={'-','+'};
	scanf("%d",&n);
	srand((unsigned) time(NULL));
		for(i=0;i<n;i++)
		printf("%2d%c%2d=___\n",rand()%10,flag[rand()%2],rand()%10);
}
void operation_two()
{
	int n,i;
	printf("请输入需要的题目:\n");
	char flag[2]={'*','/'};
	scanf("%d",&n);
	srand((unsigned) time(NULL));
	for(i=0;i<n;i++)
	printf("%2d%c%2d=___\n",rand()%100,flag[rand()%2],rand()%100);
}
void operation_three()
{
	int u,i;
	printf("请输入需要的题目\n");
	char flag[4]={'+','-','*','/'};
	scanf("%d",&u);
	srand((unsigned) time(NULL));
	for(i=0;i<u;i++);
	printf("%d %c %d  %c %d=___\n",rand()%100,flag[rand()%4],rand()%100,flag[rand()%4],rand()%100); 
}
