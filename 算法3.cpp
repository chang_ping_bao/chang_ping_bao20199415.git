#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void one_timu();
void two_timu();
void three_timu();
int main(void)
{
	int opt = -1;
	printf("========口算生成器========\n");
	printf("欢迎使用口算生成器 ：\n");
	printf("\n");
	printf("\n");

	printf("帮助信息\r\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级的题目为不超过十的加减法；\n");
	printf("二年级的题目为不超过百位的乘除法；\n");
	printf("三年级的题目为不超过百位的加减乘除混合题目。\n");
	printf("\n");
	printf("\n");
	while (opt != 0)
	{
		printf("操作列表：\n");
		printf("1)一年级  2）二年级  3）三年级\n");
		printf("4)帮助  5）退出程序\n");
		printf("\n");
		printf("\n");
		printf("请输入操作\n");
		scanf("%d", &opt);

		switch (opt)
		{
		case 1:
			printf("现在是一年级题目：\n执行完了");
			one_timu();
			break;
		case 2:
			printf("现在是二年级题目：\n执行完了");
			two_timu();
			break;
		case 3:
			printf("现在是三年级题目：\n执行完了");
			three_timu();
			break;
		case 4:
			printf("帮助信息\n");
			printf("您需要输入命令代号来进行操作，且\n");
			printf("一年级的题目为不超过十位的加减法;n");
			printf("二年级的题目为不超过百位的乘除法;\n");
			printf("三年级的题目为不超过百位的加减乘除混合题目。\n");
			break;
		case 5:opt = 0;
			printf("读鬼书\n");
			break;
		default:
			printf("Error!!!\n");
			printf("你TM蝙蝠吃多了嘛老子不晓得\n");
			break;
			printf("程序结束，欢迎下次使用\n");
		}
	}
	return 0;
}
void one_timu()
{
	int n, i, a, b;
	char symbol[2] = { '+','-' };
	char s;
	scanf("%d", &n);
	for (i = 0; i < n; i++)
	{
		a = rand() % 10;
		b = rand() % 10;
		s = symbol[rand() % 2];
		if (s == '+')
		{
			printf("%d + %d = %.6g\n", a, b, (double)a + b);
		}
		else
		{
			printf("%d - %d = %.6g\n", a, b, (double)a - b);

		}
	}
}
void two_timu()
{
	int a, b, n, i;
	char symbol[2] = { '*','/' };
	char s;
	scanf("%d", &n);
	for (i = 0; i < n; i++)
	{
		a = rand() % 99;
		b = rand() % 99;
		s = symbol[rand() % 2];
		if (s == '*')
		{
			printf("%d * %d = %.6g\n", a, b, (double)a * b);
		}
		else
		{
			printf("%d / %d = %.6g\n", a, b, (double)a / b);

		}
	}
}
void three_timu()
{
	int n, i, a, b, c;
	char symbol[4] = { '+','-','*','/' };
	char s1, s2;
	scanf("%d", &n);
	for (i = 0; i < n; i++)
	{
		a = rand() % 99;
		b = rand() % 99;
		c = rand() % 99;
		s1 = symbol[rand() % 4];
		s2 = symbol[rand() % 4];
		if (s1 == '+')
		{
			if (s2 == '+')
			{
				printf("%2d + %2d + %2d = %.6g\n", a, b, c, (double)a + b + c);
			}
			if (s2 == '-')
			{
				printf("%2d + %2d - %2d = %.6g\n", a, b, c, (double)a + b - c);
			}
			if (s2 == '*')
			{
				printf("%2d + %2d + %2d = %.6g\n", a, b, c, (double)a + b * c);
			}
			if (s2 == '/')
			{
				printf("%2d + %2d + %2d = %.6g\n", a, b, c, (double)a + b / c);
			}

		}
		if (s1 == '-')
		{
			if (s2 == '+')
			{
				printf("%2d - %2d + %2d = %.6g\n", a, b, c, (double)a + -b + c);
			}
			if (s2 == '-')
			{
				printf("%2d - %2d - %2d = %.6g\n", a, b, c, (double)a - b - c);
			}
			if (s2 == '*')
			{
				printf("%2d - %2d + %2d = %.6g\n", a, b, c, (double)a - b * c);
			}
			if (s2 == '/')
			{
				printf("%2d - %2d + %2d = %.6g\n", a, b, c, (double)a - b / c);
			}

		}
		if (s1 == '*')
		{
			if (s2 == '+')
			{
				printf("%2d * %2d + %2d = %.6g\n", a, b, c, (double)a * b + c);
			}
			if (s2 == '-')
			{
				printf("%2d * %2d - %2d = %.6g\n", a, b, c, (double)a * b - c);
			}
			if (s2 == '*')
			{
				printf("%2d * %2d + %2d = %.6g\n", a, b, c, (double)a * b * c);
			}
			if (s2 == '/')
			{
				printf("%2d * %2d + %2d = %.6g\n", a, b, c, (double)a * b / c);
			}

		}
		if (s1 == '/')
		{
			if (s2 == '+')
			{
				printf("%2d / %2d + %2d = %.6g\n", a, b, c, (double)a + b + c);
			}
			if (s2 == '-')
			{
				printf("%2d / %2d - %2d = %.6g\n", a, b, c, (double)a / b - c);
			}
			if (s2 == '*')
			{
				printf("%2d / %2d + %2d = %.6g\n", a, b, c, (double)a / b * c);
			}
			if (s2 == '/')
			{
				printf("%2d / %2d + %2d = %.6g\n", a, b, c, (double)a / b / c);
			}

		}

	}
}
